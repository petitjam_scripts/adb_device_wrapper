# Adb Device Wrapper

Wrap `adb` and `scrcpy` commands to add a device selection input when there are multiple devices.

# Usage

```sh
./adb_device_wrapper.sh adb shell
```

```sh
./adb_device_wrapper.sh scrcpy --show-touches
```

# License
Whichever one requires you to give me credit, but allows you to use it freely.
