#!/bin/bash

COMMAND=$1

FULL_COMMAND=($@)
ORIGINAL_ARGS=${FULL_COMMAND[@]:1}

DEVICES=($(adb devices | awk 'NF && NR>1 {print $1}'))

PS3="Select device: "

if [ ${#DEVICES[@]} -gt 1 ]; then
  select device in "${DEVICES[@]}"; do
    SERIAL_ARG="-s $device"
    NEW="$COMMAND $SERIAL_ARG $ORIGINAL_ARGS"

    echo $NEW
    eval $NEW

    break
  done
else
  `$@`
fi
